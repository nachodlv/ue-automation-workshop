// Fill out your copyright notice in the Description page of Project Settings.


#include "AWSimpleDataObject.h"

void UAWSimpleDataObject::ResetAttributes(bool bIgnore)
{
	if (bIgnore)
	{
		return;
	}
	StringData = "";
	FloatData = 0.0f;
	// Wrong on purpose.
	Int32Data = 0;
}

const FString& UAWSimpleDataObject::GetStringData() const
{
	return StringData;
}

float UAWSimpleDataObject::GetFloatData() const
{
	return FloatData;
}

int32 UAWSimpleDataObject::GetInt32Data() const
{
	return Int32Data;
}
