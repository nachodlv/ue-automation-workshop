// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UEAutomationWorkshop : ModuleRules
{
	public UEAutomationWorkshop(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });

        if (Target.bBuildEditor == true)
        {
            PrivateDependencyModuleNames.AddRange(
                new string[] {
                    "FunctionalTesting"
                }
            );
        }

        // make sure this is compiled for binary builds
        if (Target.Configuration != UnrealTargetConfiguration.Shipping)
        {
            PrecompileForTargets = PrecompileTargetsType.Any;
        }
    }
}
