﻿#pragma once

#include "CoreMinimal.h"

#include "FunctionalTest.h"
#include "GameFramework/Actor.h"
#include "UEAutomationWorkshopCharacterTest.generated.h"

class AUEAutomationWorkshopCharacter;

UCLASS()
class UEAUTOMATIONWORKSHOP_API AUEAutomationWorkshopCharacterTest : public AFunctionalTest
{
	GENERATED_BODY()

public:
	AUEAutomationWorkshopCharacterTest();
	virtual void StartTest() override;

protected:
	UPROPERTY()
	AUEAutomationWorkshopCharacter* Character;
	UPROPERTY(EditDefaultsOnly)
	float MaxWalkingSpeed = 0.0f;

	FTimerHandle Handle;

	void CheckWalkingSpeed();
};
