﻿#pragma once

#include "CoreMinimal.h"

#include "AWSimpleDataObject.h"
#include "Misc/AutomationTest.h"

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FAWSSimpleDataObjectDefaultGetStringTest, "AWSSimpleDataObject.DefaultGetString", EAutomationTestFlags::LowPriority | EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FAWSSimpleDataObjectDefaultGetStringTest::RunTest(const FString& Parameters)
{
	UAWSimpleDataObject* SimpleDataObject = NewObject<UAWSimpleDataObject>();
	UTEST_EQUAL(TEXT("The string in the default constructor should be \"MyString\""), SimpleDataObject->GetStringData(), TEXT("MyString"));
	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FAWSSimpleDataObjectDefaultGetFloatTest, "AWSSimpleDataObject.DefaultGetFloat", EAutomationTestFlags::LowPriority | EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FAWSSimpleDataObjectDefaultGetFloatTest::RunTest(const FString& Parameters)
{
	UAWSimpleDataObject* SimpleDataObject = NewObject<UAWSimpleDataObject>();
	UTEST_EQUAL(TEXT("The float in the default constructor should be 5.5f"), SimpleDataObject->GetFloatData(), 5.5f);
	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FAWSSimpleDataObjectDefaultGetInt32Test, "AWSSimpleDataObject.DefaultGetInt32", EAutomationTestFlags::LowPriority | EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FAWSSimpleDataObjectDefaultGetInt32Test::RunTest(const FString& Parameters)
{
	UAWSimpleDataObject* SimpleDataObject = NewObject<UAWSimpleDataObject>();
	UTEST_EQUAL(TEXT("The int in the default constructor should be 156"), SimpleDataObject->GetInt32Data(), 156);
	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FAWSSimpleDataObjectResetAttributeTest, "AWSSimpleDataObject.ResetAttributes", EAutomationTestFlags::LowPriority | EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool FAWSSimpleDataObjectResetAttributeTest::RunTest(const FString& Parameters)
{
	UAWSimpleDataObject* SimpleDataObject = NewObject<UAWSimpleDataObject>();
	SimpleDataObject->ResetAttributes();
	UTEST_EQUAL(TEXT("The string after reset attribute should be \"\""), SimpleDataObject->GetStringData(), TEXT(""));
	UTEST_EQUAL(TEXT("The float after reset attribute should be 0.0f"), SimpleDataObject->GetFloatData(), 0.0f);
	UTEST_EQUAL(TEXT("The int after reset attribute should be 0"), SimpleDataObject->GetInt32Data(), 0);
	return true;
}
