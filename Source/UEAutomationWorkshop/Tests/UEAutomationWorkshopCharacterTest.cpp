﻿


#include "UEAutomationWorkshopCharacterTest.h"

#include "UEAutomationWorkshopCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


AUEAutomationWorkshopCharacterTest::AUEAutomationWorkshopCharacterTest()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AUEAutomationWorkshopCharacterTest::StartTest()
{
	Super::StartTest();
	UWorld* World = GetWorld();
	if(!World)
	{
		return;
	}

	Character = World->SpawnActor<AUEAutomationWorkshopCharacter>(AUEAutomationWorkshopCharacter::StaticClass());
	APlayerController* Controller = UGameplayStatics::GetPlayerController(World,0);
	Controller->Possess(Character);
	UCharacterMovementComponent* CharacterMovement = Character->GetCharacterMovement();
	if(CharacterMovement)
	{
		CharacterMovement->MaxWalkSpeed = MaxWalkingSpeed;
	}
	Character->StartWalkingForward();
	const FTimerDelegate Delegate = FTimerDelegate::CreateUObject(this, &AUEAutomationWorkshopCharacterTest::CheckWalkingSpeed);
	GetWorldTimerManager().SetTimer(Handle, Delegate, 5.0f, false);
}

void AUEAutomationWorkshopCharacterTest::CheckWalkingSpeed()
{
	if(Character)
	{
		if(UCharacterMovementComponent* CharacterMovement = Character->GetCharacterMovement())
		{
			const bool bReachedSpeed = FMath::Abs(CharacterMovement->Velocity.Size() - MaxWalkingSpeed) < 0.01f;
			if(bReachedSpeed)
			{
				FinishTest(EFunctionalTestResult::Succeeded, TEXT("The Character reached the max walking speed"));
			} else
			{
				FinishTest(EFunctionalTestResult::Failed, TEXT("The Character didn't reached the walking speed"));
			}
		}

	}
}

