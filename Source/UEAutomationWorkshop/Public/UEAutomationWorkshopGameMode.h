// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UEAutomationWorkshopGameMode.generated.h"

UCLASS(minimalapi)
class AUEAutomationWorkshopGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUEAutomationWorkshopGameMode();
};



